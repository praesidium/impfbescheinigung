# Bescheinigung als Nachweis des Anspruchs auf Schutzimpfung gegen das Coronavirus SARS-CoV-2

Das **Präsidium des Studierendenparlaments der Studierendenschaft der Georg-August-Universität Göttingen** vertreten durch 

```
Sergio Perez, Präsident
Goßlerstraße 16a
37073 Göttingen
E-Mail: praesidium@stupa.uni-goettingen.de
```

bescheinigt

```
Donald Duck
Annastraße 42
23161 Entenhausen
```

folgendes:

1. Die Studierendenschaft ist gemäß §20 NHG Teilkörperschaft einer Hochschule, ist dieser somit zu zurechnen.
1. Donald Duck ist in innerhalb dieser Teilkörperschaft an einer Hochschule tätig. Vorliegend ist diese\*r am 31.02.2021 in den Dagobertausschuss des Studierendenparlaments gewählt worden und somit innerhalb dieser Teilkörperschaft tätig.

Die Voraussetzungen des §4 8. der CoronaimpfV liegen daher vor.

Göttingen, den dd.mm.2021
