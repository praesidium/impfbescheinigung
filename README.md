# Vorlage für Impfbescheinigungen

Die in diesem Repository befindliche Vorlage wird vom StuPa-Präsidium 2021 verwendet, um Impfbescheinigungen für Personen der zentralen studentischen Selbstverwaltung nach Prioritätsstufe 3 auszustellen.

## Installation

Die Vorlage liegt im **markdown**-Format vor und wird mittels **pandoc** unter Verwendung eines speziell angefertigten Templates in **pdf** umgewandelt.
Das zu Grunde liegende Template ist unter [https://gitlab.gwdg.de/praesidium/protokolltemplate](https://gitlab.gwdg.de/praesidium/protokolltemplate) verfügbar und ist eine minimale Anpassung des Templates welches im Digitalisierungsreferates des AStA 2020 entwickelt wurde.
Das Projekt des AStA ist unter [https://gitlab.gwdg.de/asta/dnd-referat/protokolltemplate](https://gitlab.gwdg.de/asta/dnd-referat/protokolltemplate) erreichbar.
Vor erfolgreichen Nutzung der Vorlage müssen folgende Schritte durchgeführt werden:

1. [pandoc](https://pandoc.org/) installieren
2. [pandoc-Template](https://gitlab.gwdg.de/praesidium/protokolltemplate) gemäß [eigener Anleitung](https://gitlab.gwdg.de/praesidium/protokolltemplate#usage) einrichten

## Nutzung

Folgende Schritte führen unter Linux von der md-Datei zu der hübschen pdf-Datei. Unter Windows und MAC OS wurden die Schritte nicht getestet und funktionieren in dieser Form wahrscheinlich nicht.

1. Eine Kopie der beiden Dateien `vorlage.md` und `vorlage_meta.md` erstellen.
2. In den kopierten Dateien die Daten nach eigenen Bedürfnissen anpassen (personenbezogene Daten, Position und Datum). Hierbei ist zu beachten, dass in der Metadaten-Datei die Variablennamen nicht zum zugewiesenen Inhalt passen. An dieser Stelle handelt es sich nur um Namen des internen Template-Codes, welche keinen weiteren Kontext haben.
3. In einem Terminal in das Verzeichnis der md-Dateien wechseln. Dort müssen sich auch die Dateien `logo_stupa.svg` und `AStA-Protokolle-Background-Inner-Light-V01.pdf` befinden.

```
cd path/to/md-files/
```

4. Mit folgendem Befehl pandoc aufrufen. Dabei `vorlage` durch die entsprechenden Dateinamen der Kopien ersetzen und `stupa2021` gegebenenfalls durch den Namen eines anderen Templates ersetzen.

```
pandoc --template stupa2021 -V lang=de vorlage.md vorlage_meta.md -o vorlage.pdf
```

5. Sind keine Fehler aufgetreten und ist das pandoc-Template korrekt eingerichtet ist `vorlage.pdf` jetzt die fertige Impfbescheinigung.

## Anpassungen

Für Anpassungen an das eigene Parlament empfiehlt es sich, in dem pandoc-Template, welches in LaTeX geschrieben ist, die Vorkommen von *StuPa Präsidium Uni Göttingen* bzw. *AStA* durch den Namen eures Parlamentes zu ändern.
Außerdem sollte das Logo ausgetauscht werden, welches im Dateiformat svg sein sollte.

## Lizenz

Die Dateien in diesem Projekt sind unter GNU AFFERO GENERAL PUBLIC LICENSE (AGPL) lizenziert.
Mehr Details stehen in der [LICENSE](LICENSE).
