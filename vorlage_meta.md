---
reduce-color: true
titlepage: false
document-type: "Bescheinigung"
institution: "2021-mm-dd"
event-type: ""
date: ""
lang: "de"
title: "Bescheinigung als Nachweis des Anspruchs auf Schutzimpfung gegen das Coronavirus SARS-CoV-2"  # for the header, default: empty
...
